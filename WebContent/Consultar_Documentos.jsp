<%@page import="org.apache.catalina.startup.Tomcat.ExistingStandardWrapper"%>
<%@page import="com.mongodb.client.result.DeleteResult"%>
<%@page import="com.mongodb.client.MongoCollection"%>
<%@page import="com.mongodb.client.result.UpdateResult"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.mongodb.client.MongoCursor"%>
<%@ page import="com.mongodb.client.MongoDatabase"%>
<%@ page import="com.mongodb.client.model.Filters"%>
<%@ page import="org.bson.Document"%>
<%@ page import="org.bson.conversions.Bson"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.TimeZone"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.net.URL"%>
<%@ page import="java.net.URLConnection"%>
<%@ page import="config.DBConfig"%>


<html>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  
<body>


<div class="container">

		<br></br>


			<div class="row">
			
			<div class="col-lg-1 col-centered">
				<a href="index.jsp" class="btn btn-default" role="button"> 
					<span class="btn-label"><i class="glyphicon glyphicon-home"></i></span>
					Home
				</a>
			</div>
						
			<center><h2>Consultar Por DNI </h2></center>
			
			<br></br>
			
			<form action="Consultar_Documentos.jsp" method="get" class="form-horizontal">	
				<div class="form-group">
					<label for="identifier" class="col-sm-2 control-label">DNI</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="dni" id="ruc"
							value="">
					</div>
				</div>

				<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
				<input type="submit" class="btn btn-info" id="consultar" value="Consultar" /></div>					
				</div>				
			</form>	
			
							
			
			</div>


		<br></br>


		<%	//jalando los datos name	
		
		String dni = request.getParameter("dni");

		%>
				
		<%
		DBConfig dbConfig = new DBConfig();
		
		
		MongoDatabase DBO=null;
		
		DBO = dbConfig.CNXMongoAtlas();
		
		if(DBO!= null){
			System.out.println("Conexion Ok");
		}else{
			System.out.println("Error Conexión");
		}
		
		Document document = null;
		String respuesta = "";
		Bson filter = null;
		
		Bson Fdni = new Document("dni", dni);
	

		filter=Filters.and(Fdni);	
				
		MongoCursor<Document> result = DBO.getCollection("empleado").find(filter).iterator();
		
		System.out.println(result);
		
		boolean exist=false;		
		
		while (result.hasNext()) {
			
			
			document = result.next();	
			System.out.println(document.toJson().toString());
			String identifier=document.getString("identifier");
			String nombre = document.getString("nombre");
			String apellido = document.getString("apellido");
			String documento = document.getString("dni");			
			Boolean enabled = document.getBoolean("Enabled");
	
			
			  //LLAMA FECHA EMISION
			Date IssueDate=document.getDate("IssueDate");		  
//			SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
            SimpleDateFormat sdfPeru = new SimpleDateFormat("yyyy-MM-dd");
			sdfPeru.setTimeZone(TimeZone.getTimeZone("America/Peru/Lima"));
			String fechaPeru = sdfPeru.format(IssueDate);

			
			
			
			out.println("<table class='table table-sm'>");			
			out.println("<thead>");
			out.println("<tr>");
			out.println("<th scope='col'>Identifier</th>");
			out.println("<th scope='col'>Nombre</th>");
			out.println("<th scope='col'>Apellido</th>");			
			out.println("<th scope='col'>Documento</th>");
			out.println("<th scope='col'>Enabled</th>");			
			out.println("<th scope='col'>IssueDate</th>");
	
			out.println("<tr>");
			out.println("</thead>");

			out.println(" <tbody>");
							 
			out.println("<td>" + identifier + "</td>");
			out.println("<td>" + nombre + "</td>");
			out.println("<td>" + apellido + "</td>");
			out.println("<td>" + documento + "</td>");
			out.println("<td>" + enabled + "</td>");
			out.println("<td>" + fechaPeru + "</td>");			

			out.println(" </tbody>");
			out.println("</table>");
			


			
		}
		
		%>
		
		


</div>
	

</body>

		

</html>